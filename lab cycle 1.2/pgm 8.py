# Python program to find GCD of two numbers using the while loop
print("*******programe to find the gcd of two numbers*******")
print ("-----Enter the two positive integer numbers-----")
p = int (input ("Enter 1st the number:"))
q = int (input ("Enter 2nd the number:"))

while p != q:
	if p > q:
		p = p-q
	else:
		q = q-p

print ("\nThe GCD number is: ", p)
